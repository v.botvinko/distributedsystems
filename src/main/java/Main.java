import org.apache.commons.compress.compressors.CompressorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Main {
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);
    public static void main(String[] args){
        String filename = "RU-NVS.osm.bz2";
        try(InputStream inputStream = Main.class.getClassLoader().getResourceAsStream(filename)) {
            if (inputStream != null) {
                InputStreamReader inputStreamReader = BZip2Reader.getBzip2Reader(inputStream);
                new StAXParser(inputStreamReader).ParseXML();
            }
        } catch (CompressorException | XMLStreamException | IOException e) {
            LOGGER.error(e.getMessage());
        }
    }
}
