import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class StAXParser implements AutoCloseable {
    private static final XMLInputFactory FACTORY = XMLInputFactory.newInstance();
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);
    private final XMLEventReader reader;

    public StAXParser(InputStreamReader inputStreamReader) throws XMLStreamException {
        reader = FACTORY.createXMLEventReader(inputStreamReader);
    }

    public void ParseXML() {
        Map<String, Integer> editsMap = new HashMap<>();
        Map<String, Integer> uniqueTags = new HashMap<>();
        LOGGER.info("Starts xml parsing");
        try {
            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();
                if (event.isStartElement()) {
                    StartElement startElement = event.asStartElement();
                    if ("node".equals(startElement.getName().getLocalPart())) {
                        processEditors(startElement, editsMap);
                    }
                    if ("tag".equals(startElement.getName().getLocalPart())) {
                        processTags(startElement, uniqueTags);
                    }
                }
            }
            LOGGER.info("Editors:");
            printMap(sortByValues(editsMap));
            LOGGER.info("Tags:");
            printMap(sortByValues(uniqueTags));
        } catch (XMLStreamException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private void processTags(StartElement startElement, Map<String, Integer> uniqueTags) {
        Iterator<Attribute> tags = startElement.getAttributes();
        var key = startElement.getAttributeByName(new QName("k"));
        var value = startElement.getAttributeByName(new QName("v"));
        if (key != null && key.getValue().equals("name") && value != null) {
            uniqueTags.merge(value.getValue(), 1, Integer::sum);
        }
    }

    private void processEditors(StartElement startElement, Map<String, Integer> editors) {
        Attribute user = startElement.getAttributeByName(new QName("user"));
        if (user != null) {
            editors.merge(user.getValue(), 1, Integer::sum);
        }
    }

    private HashMap<String, Integer> sortByValues(Map<String, Integer> map) {
        return map.entrySet()
                .stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }

    private void printMap(Map <String, Integer> map) {
        map.forEach((key, value) -> LOGGER.info(key + ":" + value));
    }

    @Override
    public void close() throws Exception {
        if (reader != null) {
            reader.close();
        }
    }
}
