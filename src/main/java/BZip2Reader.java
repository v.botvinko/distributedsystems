import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

public class BZip2Reader {
    public static InputStreamReader getBzip2Reader(@NotNull InputStream inputStream) throws CompressorException {
        return new InputStreamReader(new CompressorStreamFactory().
                createCompressorInputStream(new BufferedInputStream(inputStream)));
    }
}
